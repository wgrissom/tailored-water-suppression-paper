% Tailored spiral in-out RF pulse design for water suppression in MRSI
% 
% From the manuscript:
% Tailored Spiral In-Out Spectral-Spatial Water Suppression Pulses for 
%   Magnetic Resonance Spectroscopic Imaging
% by Jun Ma, Carrie Wismans, Zhipeng Cao, Dennis W.J. Klomp, Jannie P. Wijnen 
% and William A. Grissom, submitted to Magnetic Resonance in Medicine, 2017
% 
% For further assistance contact Jun (Martin) Ma (jun.ma@vanderbilt.edu) 
% or Will Grissom (will.grissom@vanderbilt.edu)

% Add John Pauly's RF tools to get dzrf_angle and b2rf
addpath util/
addpath util/rf_tools/
addpath util/rf_tools/mex5/

dataPath = 'data/';

flip_0 = 80; % flip angle in deg, of the first subpulse
flip_1 = 80; % of the second subpulse
flip_2 = 145;% of the third subpulse
% 80, 80, 145 is the default combination in the Philips 7T WET sequence

bw = 220; % Hz, width of passband
totalDur = 26; % ms, duration of pulse
subpulseDur = 1/(5.2*7*42.58)*1000; % 5.2 ppm spectral separation between repeated passbands
NSubPulse = floor(totalDur/subpulseDur)+1;
d1 = 0.001; % passband ripple (this is the target % flip angle error in water)
d2 = 0.01; % stopband ripple (this is the target % flip angle error outside of water)
dt = 6.4e-6; % seconds, dwell time


powReg = 10^2.5; % RF power regularization parameter
%(10^2.5 is the optimum obtained from multiple experiments, for TR = 300 ms)
dimxy = [32 32]; % dimensions of of design grid in (x,y,z)

trajType = 'measAll'; % final measured spiral in-out trajectory, after pre-distortion
% Other options: 'inout'; % ideal sprial in-out trajectory;
%                'in'; % ideal spiral in trajectory;

rootFlipEnvelope = true; % switch to root flip the spectral envelope

kPhsInc = 0; % phase increment between consecutive spirals (only compatible with full design)
FOVsubsamp = 10; % cm, xfov of spiral (10 is best for phantom, 12 for vandy brain, 10 for janni_ang/pfc)
dgdtmax = 18000; % slew (g/cm/s)
gmax = 4; % g/cm



% calculate spectral time-bandwidth product
tbw = 15.25/14*bw*totalDur/1000;% time-bandwidth product


load([dataPath 'b1b0maps.mat']);
load([dataPath 'DesignMask.mat']);

fovx=fov(1);
fovy=fov(2);

sens=b1map;

fmap1=b0map; %store fmap for simulation
fmap=0*b0map;%eliminate B0 information for design.

fmax = 250;
mask(fmap < -fmax | fmap > fmax) = false;
sens(fmap < -fmax | fmap > fmax) = 0;
fmap(fmap < -fmax | fmap > fmax) = 0;

% try a spiral
deltax = 1; % spatial resolution of trajectory, cm

forwardspiral = 0;
dorevspiralramp = 0;
traj = 'spiral';
get_traj;

switch trajType
    
    case 'measAll'
        
        % spiral in-out, measured and grip'd
        traj = load([dataPath 'optimizedGradient.mat']);
        g1 = traj.g_grip_sameArea(:,:,end);
        %k1 = traj.k_meas;k1 = k1 - repmat(k1(50,:),[100 1]); % set to DC in middle
        load([dataPath 'measured_k'],'k1');
        rfmask1 = traj.designvars.rfmask1;
        
    case 'inout'
        
        % spiral in-out (each one is 1/2 subpulse duration)
        
        % strip off samples until we get just under subpulsedur/2,
        % accounting for ramp
        g = g(end-ceil(subpulseDur/2/(dt*1000))+1:end,:);
        k = k(end-ceil(subpulseDur/2/(dt*1000))+1:end,:);
        nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        while (size(g,1) + nRamp)*dt*1000 > subpulseDur/2
            g = g(2:end,:);
            k = k(2:end,:);
            nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        end
        gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
        g = [gRamp;g];
        g1 = [g; -flipud(g)];
        
        rfmask = [false(nRamp,1);true(size(k,1),1)];
        k = [zeros(nRamp,2);k];
        k1 = [k; flipud(k)];
        rfmask1 = [rfmask; flipud(rfmask)];
        
    case 'in'
        % spiral in only
        
        % strip off samples until we get just under subpulsedur/2,
        % accounting for ramp down AND rewinder
        g = g(end-floor(subpulseDur/(dt*1000))+1:end,:);
        k = k(end-floor(subpulseDur/(dt*1000))+1:end,:);
        nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
        gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
        for ii = 1:2
            nTrap(ii) = length(dotrap(sum([gRamp(:,ii);g(:,ii)])*dt,gmax,dgdtmax,dt));
        end
        while (size(g,1) + nRamp + max(nTrap))*dt*1000 > subpulseDur
            g = g(2:end,:);
            k = k(2:end,:);
            nRamp = ceil(max(abs(g(1,:)))/dgdtmax/dt);
            gRamp = (0:nRamp-1)'/(nRamp)*g(1,:);
            for ii = 1:2
                nTrap(ii) = length(dotrap(sum([gRamp(:,ii);g(:,ii)])*dt,gmax,dgdtmax,dt));
            end
        end
        gTrap = {};
        for ii = 1:2
            gTrap{ii} = -dotrap(sum([gRamp(:,ii);g(:,ii)])*dt,gmax,dgdtmax,dt);
        end
        maxNTrap = max(nTrap);
        g1 = [[zeros(maxNTrap-length(gTrap{1}),1);gTrap{1}(:)] [zeros(maxNTrap-length(gTrap{2}),1);gTrap{2}(:)];gRamp;g];
        
        rfmask1 = [false(nRamp+maxNTrap,1);true(size(k,1),1)];
        k1 = [zeros(nRamp+maxNTrap,2);k];
        
end

% build up full gradient/k trajectories
kPhs = (0:floor(totalDur/subpulseDur))*kPhsInc;
k = [];
g = [];
for ii = 1:length(kPhs)
    k = [k; (k1(:,1) + 1i*k1(:,2))*exp(1i*kPhs(ii))];
    g = [g; (g1(:,1) + 1i*g1(:,2))*exp(1i*kPhs(ii))];
end

k = [real(k) imag(k)];
g = [real(g) imag(g)];
rfmask = repmat(rfmask1,[floor(totalDur/subpulseDur)+1 1]);

Nt = size(k,1);


% Design the sptial subpulse and spectral envelope seperately

% first design a spatial pulse
ti = ((0:length(k1)-1) - length(k1))*dt;

[x,y] = ndgrid(-fovx/2:fovx/size(mask,1):fovx/2-fovx/size(mask,1),...
    -fovy/2:fovy/size(mask,1):fovy/2-fovy/size(mask,2));

A = exp(1i*2*pi*((x(mask(:))*k1(:,1).' + y(mask(:))*k1(:,2).') + ...
    fmap(mask(:))*ti(:)'));
A = bsxfun(@times,A,sens(mask)./median(abs(sens(mask))));
R = diag(sqrt(powReg*sum(mask(:))/sum(rfmask1))*double(rfmask1)+10^12*double(~rfmask1));
Ainv = (A'*A + R'*R)\A';

d = mask;

% set initial target phase as
phs = 0*d;
rf1 = zeros(size(A,2),1);
cost = [Inf norm(double(d(mask)))^2];
% build regularization matrix to force zero RF on ramps and penalize
% integrated power. Also normalize by # spatial locs and # RF time
% points.
while cost(end) < 0.9999*cost(end-1)
    
    rf1 = Ainv*d(mask);
    
    m = embed(A*rf1,mask);
    phs = angle(m);
    d = abs(d).*exp(1i*phs); % MLS phase update
    res = d(mask)-m(mask);reg = R*rf1;
    cost(end+1) = real(res'*res) + real(reg'*reg);
    fprintf('Iteration %d cost: %d\n',length(cost),cost(end));
    
end
% let user know how the RF power and error terms balance out
rfPowReg = R*rf1;rfPowReg = real(rfPowReg'*rfPowReg);
exErr = A*rf1 - d(mask);exErr = real(exErr'*exErr);
fprintf('At exit, error is %d and power reg is %d\n',exErr,rfPowReg);

rf1 = rf1./(2*pi*4258*dt); % scale to target flip (1 radian)
rf1 = rf1./median(abs(sens(mask))); % normalize by median of B1 map to
% improve regularization
% consistency
% end designing spatial pulse


% Start the dsign of spectral envelope
% get min-phase SLR pulse weights
disp('Designing SLR envelope')

[rfw,b] = dzrf_angle(NSubPulse,tbw,'sat','min',d1,d2,flip_2);
rfw = real(rfw);

if rootFlipEnvelope
    
    disp('Root-flipping SLR envelope')
    
    % root flip the envelope to minimize peak B1/SAR
    b = b./max(abs(freqz(b)));
    d1flip = d1/2;
    
    
    b = b*sin(pi/180*flip_2/2 + atan(d1flip*2)/2); % scale to target flip angle
    
    r = roots(b); % calculate roots of single-band beta polynomial
    % sort the roots by phase angle
    [~,idx] = sort(angle(r));
    r = r(idx);r = r(:);
    r = leja_fast(r);
    
    candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tbw/length(b)*pi;% ...
    
    rfall = [];
    for ii = 1:2^sum(candidates)
        
        % convert this ii to binary pattern
        doflip = de2bi(ii-1);
        % add zeros for the rest of the passbands
        doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
        % embed in all-roots vector
        tmp = zeros(length(b)-1,1);
        tmp(candidates) = doflip;
        doflip = tmp;
        % flip those indices
        rt = r(:);
        rt(doflip == 1) = conj(1./rt(doflip == 1));
        % get polynomial coefficients back from flipped roots
        tmp = poly(rt);
        tmp = tmp./max(abs(freqz(tmp)));
        % store the rf
        
        rfall = [rfall col(b2rf(tmp(:)*sin(pi/180*flip_2/2 + atan(d1flip*2)/2)))];
        
    end
    
    % take RF with min peak amplitude
    [~,ind] = min(max(abs(rfall),[],1));
    rfw = rfall(:,ind);
    
end

% apply the weights to build the whole pulse
rf = kron(rfw(:),rf1(:));

% plot the pulse and the pattern
figure(1)
subplot(313)
plot(1000*dt*(0:length(rf)-1),abs(rf)*nomb1*100); % pulse in uT (nomb1 is nominal b1 amplitude with with B1 map was measured)
xlabel 'ms'
ylabel 'RF demand (uT)'
title(strcat({'RF_2:'},{' '},{num2str(flip_2)},{' degree'}))

% Plot normalized flip angle simulated from WET and Sprial SPSP
% Excitation pattern from WET is directly proportional to B1 map
% Excitation pattern from Sprial SPSP is simulated from designed spatial subpulse
figure(2)
subplot(2,2,1)
imagesc(abs(1-sens./mean(sens(mask)).*mask)*100,[0 100]);
title('Hardpulse error')
colormap jet
c = colorbar;
ylabel(c,'Simulated Excitation Error')
title(c,'%')
axis off
axis image
subplot(2,2,2)
imagesc(abs(1-abs(m))*100,[0 100])
title('Spiral in-out error')
axis off
colormap jet
c = colorbar;
ylabel(c,'Simulated Excitation Error')
title(c,'%')
axis image

%% Designing the first subpulse _0

% get min-phase SLR pulse weights
disp('Designing SLR envelope')
[rfw,b] = dzrf_angle(NSubPulse,tbw,'sat','min',d1,d2,flip_0);
rfw = real(rfw);

if rootFlipEnvelope
    
    disp('Root-flipping SLR envelope')
    
    % root flip the envelope to minimize peak B1/SAR
    b = b./max(abs(freqz(b)));
    d1flip = d1/2;
    b = b*sin(pi/180*flip_0/2 + atan(d1flip*2)/2); % scale to target flip angle
    r = roots(b); % calculate roots of single-band beta polynomial
    % sort the roots by phase angle
    [~,idx] = sort(angle(r));
    r = r(idx);r = r(:);
    r = leja_fast(r);
    
    candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tbw/length(b)*pi;% ...
    
    rfall = [];
    for ii = 1:2^sum(candidates)
        
        % convert this ii to binary pattern
        doflip = de2bi(ii-1);
        % add zeros for the rest of the passbands
        doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
        % embed in all-roots vector
        tmp = zeros(length(b)-1,1);
        tmp(candidates) = doflip;
        doflip = tmp;
        % flip those indices
        rt = r(:);
        rt(doflip == 1) = conj(1./rt(doflip == 1));
        % get polynomial coefficients back from flipped roots
        tmp = poly(rt);
        tmp = tmp./max(abs(freqz(tmp)));
        % store the rf
        rfall = [rfall col(b2rf(tmp(:)*sin(pi/180*flip_0/2 + atan(d1flip*2)/2)))];
        
    end
    
    % take RF with min peak amplitude
    [~,ind] = min(max(abs(rfall),[],1));
    rfw = rfall(:,ind);
    
end

% apply the weights to build the whole pulse
rf = kron(rfw(:),rf1(:));

% plot the pulse and the pattern
figure(1)
subplot(311)
plot(1000*dt*(0:length(rf)-1),abs(rf)*nomb1*100); % pulse in uT
xlabel 'ms'
ylabel 'RF demand (uT)'
title(strcat({'RF_0:'},{' '},{num2str(flip_0)},{' degree'}))

%% Designing the second subpulse _1

if flip_1~=flip_0
    % If the second subpulse has the same flip angle as the first subpulse,
    % The spectral envelope will be the same,
    % We don't need to design the envelope again.
    
    % get min-phase SLR pulse weights
    disp('Designing SLR envelope')
    [rfw,b] = dzrf_angle(NSubPulse,tbw,'sat','min',d1,d2,flip_1);
    rfw = real(rfw);
    
    if rootFlipEnvelope
        
        disp('Root-flipping SLR envelope')
        
        % root flip the envelope to minimize peak B1/SAR
        b = b./max(abs(freqz(b)));
        d1flip = d1/2;
        b = b*sin(pi/180*flip_1/2 + atan(d1flip*2)/2); % scale to target flip angle
        r = roots(b); % calculate roots of single-band beta polynomial
        % sort the roots by phase angle
        [~,idx] = sort(angle(r));
        r = r(idx);r = r(:);
        r = leja_fast(r);
        
        candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tbw/length(b)*pi;% ...
        
        rfall = [];
        for ii = 1:2^sum(candidates)
            
            % convert this ii to binary pattern
            doflip = de2bi(ii-1);
            % add zeros for the rest of the passbands
            doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
            % embed in all-roots vector
            tmp = zeros(length(b)-1,1);
            tmp(candidates) = doflip;
            doflip = tmp;
            % flip those indices
            rt = r(:);
            rt(doflip == 1) = conj(1./rt(doflip == 1));
            % get polynomial coefficients back from flipped roots
            tmp = poly(rt);
            tmp = tmp./max(abs(freqz(tmp)));
            % store the rf
            rfall = [rfall col(b2rf(tmp(:)*sin(pi/180*flip_1/2 + atan(d1flip*2)/2)))];
            
        end
        
        % take RF with min peak amplitude
        [~,ind] = min(max(abs(rfall),[],1));
        rfw = rfall(:,ind);
        
    end
end

% apply the weights to build the whole pulse
rf = kron(rfw(:),rf1(:));

% plot the pulse and the pattern
figure(1)
subplot(312)
plot(1000*dt*(0:length(rf)-1),abs(rf)*nomb1*100); % pulse in uT
xlabel 'ms'
ylabel 'RF demand (uT)'
title(strcat({'RF_1:'},{' '},{num2str(flip_1)},{' degree'}))


%%


% Bloch-simulate over entire frequency range
disp('Bloch-simulating entire pulse')


[rfw,b] = dzrf_angle(NSubPulse,tbw,'sat','min',d1,d2,90);
rfw = real(rfw);

if rootFlipEnvelope
    
    disp('Root-flipping SLR envelope')
    
    % root flip the envelope to minimize peak B1/SAR
    b = b./max(abs(freqz(b)));
    d1flip = d1/2;
    b = b*sin(pi/180*flip_1/2 + atan(d1flip*2)/2); % scale to target flip angle
    r = roots(b); % calculate roots of single-band beta polynomial
    % sort the roots by phase angle
    [~,idx] = sort(angle(r));
    r = r(idx);r = r(:);
    r = leja_fast(r);
    
    candidates = abs(1-abs(r)) > 0.004 & abs(angle(r)) < tbw/length(b)*pi;% ...
    
    rfall = [];
    for ii = 1:2^sum(candidates)
        
        % convert this ii to binary pattern
        doflip = de2bi(ii-1);
        % add zeros for the rest of the passbands
        doflip = [doflip(:); zeros(sum(candidates)-length(doflip),1)];
        % embed in all-roots vector
        tmp = zeros(length(b)-1,1);
        tmp(candidates) = doflip;
        doflip = tmp;
        % flip those indices
        rt = r(:);
        rt(doflip == 1) = conj(1./rt(doflip == 1));
        % get polynomial coefficients back from flipped roots
        tmp = poly(rt);
        tmp = tmp./max(abs(freqz(tmp)));
        % store the rf
        rfall = [rfall col(b2rf(tmp(:)*sin(pi/180*flip_1/2 + atan(d1flip*2)/2)))];
        
    end
    
    % take RF with min peak amplitude
    [~,ind] = min(max(abs(rfall),[],1));
    rfw = rfall(:,ind);
    
end


% apply the weights to build the whole pulse
rf = kron(rfw(:),rf1(:));


simFreqs = -1500:10:1500; % Hz, range of frequencies to simulate the subpulse over in the separable design
NfSim = length(simFreqs);


Nt = size(k,1);
ti = ((0:Nt-1) - Nt)*dt;
k(:,3) = ti;

omdt = zeros([1 1 NfSim]);
omdt =omdt(:);
[xx,yy,ff] = ndgrid(0,0,simFreqs);
xx = [xx(:) yy(:) ff(:)];
sensBloch = repmat(sens(16,16),[1 1 NfSim]);
sensBloch = complex(sensBloch(:));
garea = [zeros(1,3);diff(k(:,1:3),1,1)/dt/4258]*2*pi*dt*4258;
[a,b] = blochsim_optcont_mex(ones(Nt,1),ones(Nt,1),rf(:)*dt*4258*2*pi,sensBloch,...
    garea,xx,omdt,2);
Mz = (1-2*abs(b)).^2;

figure(2)
subplot(2,2,[3 4])
plot(simFreqs,Mz)
title('Simulated Spectral Profile at the Center Pixel')
xlabel('Frequency (Hz)')
ylabel('Mz')

%%
load('CSIdata_v6150.mat')
load('Maskout_v6150.mat')
figure(3)
subplot(121)
imagesc((intspokes_CHESS./intspokes_Water.*maskout)'*100,[0 15]);
axis image
axis off
title('WET')
c = colorbar;
ylabel(c,'Measured Water Residual')
title(c,'%')
axis image
subplot(122)
imagesc((intspokes_TRF./intspokes_Water.*maskout)'*100,[0 15]);
axis image
axis off
c = colorbar;
title('Spiral SPSP')
ylabel(c,'Measured Water Residual')
title(c,'%')