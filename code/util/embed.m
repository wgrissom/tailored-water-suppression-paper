function out = embed(in,mask)

out = zeros(size(mask));
out(mask) = in;
